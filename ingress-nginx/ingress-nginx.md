# Ingress
20.76.184.185.nip.io

An ingress is a DNS name for a service. Ingresses may provide load balancing, SSL termination and rate limiting.
One of the most basic functionalities is providing tsl termination.

First, let's create an arbitrary deployment:
```
$ kubectl create deployment hello-test --image=gcr.io/google-containers/echoserver:1.8
$ kubectl expose deployment hello-test --port=8080
```

As we can check with
```
$ kubectl get svc,deploy
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/hello-test   ClusterIP   10.0.237.0   <none>        8080/TCP   41s

NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello-test   1/1     1            1           74s
```
we created and exposed a simple deployment.

Now let's make sure we can reach it from outside the cluster.
Let's create a simple ingress. First, we have to replace the string `CHANGEME` with your name
in the yaml file, as we can't all use the same path.

```
$ sed -i 's/CHANGEME/YOUR_NAME/' ./ingress.yaml
```

Next, let's apply the ingress:
gcr.io/google-containers/echoserver
```
$ kubectl apply -f ingress.yaml
ingress.networking.k8s.io/hello-world-ingress created
```

To find out the ingress controllers external IP address, we have to look in the `ingress-basic` namespace:

```
$ kubectl get svc --namespace kube-networking
NAME                                               TYPE           CLUSTER-IP    EXTERNAL-IP      PORT(S)                      AGE
nginx-ingress-ingress-nginx-controller             LoadBalancer   10.0.42.158   20.76.184.185   80:31421/TCP,443:31366/TCP   26m
nginx-ingress-ingress-nginx-controller-admission   ClusterIP      10.0.42.1     <none>           443/TCP                      26m
```

The ingress controller has an associated `LoadBalancer` service with an external ip. We can call out
deployment using this ip and the path we just specified.

```
curl <IP>.nip.io/YOUR_NAME/ | grep request_uri
        request_uri=http://20.76.184.185.nip.io:8080/YOUR_NAME/
```

As you have probably seen in the ingress file, there is some rewrite magic going on. Create the ingress defined in `ingress-static.yaml`.

```
$ sed -i 's/CHANGEME/YOUR_NAME/' ./ingress-static.yaml
$ kubectl apply -f ingress-static.yaml
ingress.networking.k8s.io/hello-world-ingress-static created
```

---
**Exercise**

Compare the output of

* `curl 20.76.184.185.nip.io/YOUR_NAME/ | grep request_uri`
* `curl 20.76.184.185.nip.io/YOUR_NAME/hello-world/  | grep request_uri`
* `curl 20.76.184.185.nip.io/YOUR_NAME/wrong/  | grep request_uri`
* `curl 20.76.184.185.nip.io/YOUR_NAME/static/  | grep request_uri`
* `curl 20.76.184.185.nip.io/YOUR_NAME/static/test  | grep request_uri`
---

Sometimes, it is usefull to view the ingress controller logs for debugging:
```
$ kubectl logs nginx-ingress-ingress-nginx-controller-55d6b7dc57-zg2hg --namespace kube-networking
```


## Clean Up
```
$ kubectl delete deployment hello-test
$ kubectl delete service hello-test
```
Look at the folder `TwoServices` for the next Ingress exercise
