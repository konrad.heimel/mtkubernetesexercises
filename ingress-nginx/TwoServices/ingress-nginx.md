# Ingress

First, let's create an two deployments:
```
$ kubectl create deployment hello-one --image=gcr.io/google-samples/hello-app:1.0
$ kubectl expose deployment hello-one --port=8080
$ kubectl create deployment hello-two --image=gcr.io/google-samples/hello-app:2.0
$ kubectl expose deployment hello-two --port=8080
```

Update the Ingress File with your Name
```
$ sed -i 's/CHANGEME/YOUR_NAME/' ./ingress.yaml
```

Now look at the `ingress.yaml` file. Try to find a way to make the `hello-two` deployment available under a different path and apply the Ingress to test it.

```
$ kubectl apply -f ingress.yaml
```

## Clean Up
```
$ kubectl delete deployment hello-one
$ kubectl delete service hello-one
$ kubectl delete deployment hello-two
$ kubectl delete service hello-two
```
