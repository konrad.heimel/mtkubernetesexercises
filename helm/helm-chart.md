# Helm

## Helm Chart Repositories
To get started with helm, you need to first add a chart repository:
```
$ helm repo add bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories
```

First let's verify that the `bitnami` repositories is installed
```
$ helm repo list
NAME    URL                               
bitnami https://charts.bitnami.com/bitnami        
```

To view the charts available in the repository `bitnami`, type;
```
helm search repo bitnami
NAME                                            CHART VERSION   APP VERSION     DESCRIPTION                                       
bitnami/bitnami-common                          0.0.9           0.0.9           DEPRECATED Chart with custom templates used in ...
bitnami/airflow                                 10.3.1          2.1.2           Apache Airflow is a platform to programmaticall...
bitnami/apache                                  8.6.3           2.4.48          Chart for Apache HTTP Server                      
bitnami/argo-cd                                 1.0.3           2.0.5           Declarative, GitOps continuous delivery tool fo...
bitnami/aspnet-core                             1.3.16          3.1.18          ASP.NET Core is an open-source framework create...
bitnami/cassandra                               8.0.3           4.0.0           Apache Cassandra is a free and open-source dist...
bitnami/cert-manager                            0.1.19          1.5.3           Cert Manager is a Kubernetes add-on to automate...
bitnami/common                                  1.8.0           1.8.0           A Library Helm Chart for grouping common logic ...
bitnami/concourse                               0.1.4           7.4.0           Concourse is a pipeline-based continuous thing-...
bitnami/dataplatform-bp1                        7.0.0           1.0.0           OCTO Data platform Kafka-Spark-Solr Helm Chart    
bitnami/dataplatform-bp2                        5.0.0           1.0.0           OCTO Data platform Kafka-Spark-Elasticsearch He...
bitnami/discourse                               4.2.12          2.7.7           A Helm chart for deploying Discourse to Kubernetes
bitnami/dokuwiki                                11.2.5          20200729.0.0    DokuWiki is a standards-compliant, simple to us...
...
```

Update the chart repository:
```
$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "bitnami" chart repository
```

## Exploring a helm chart
As an example, we will install dokuwiki from `bitnami/dokuwiki`. But before we install it, we will look at
the configuration options, the chart provides.

```
$ helm show chart bitnami/dokuwiki 
annotations:
  category: Wiki
apiVersion: v2
appVersion: 20200729.0.0
dependencies:
- name: common
  repository: https://charts.bitnami.com/bitnami
  version: 1.x.x
...
```

A helm chart is configured using a `.yaml` file. The default configuration as well as usually some documentation is stored
in the `values.yaml` file.`
```
$  helm show values bitnami/dokuwiki | more
## @section Global parameters
## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global Docker image parameters: imageRegistry, imagePullSecrets and storageClass

## @param global.imageRegistry Global Docker image registry
## @param global.imagePullSecrets Global Docker registry secret names as an array
## @param global.storageClass Global StorageClass for Persistent Volume(s)
##
global:
  imageRegistry: ""
  ## E.g.
  ## imagePullSecrets:
  ##   - myRegistryKeySecretName
  ##
  imagePullSecrets: []
  storageClass: ""
...
```

---
**Note**: 
You can exit `more` by typing `:q`
---

As you can see, there are lots of configuration options. Luckily, the developers of the chart provided us with some
example values.yaml files to be found. First let's download an unpacked version of the chart.

```
$ helm pull bitnami/dokuwiki --untar
```

There should now be a folder `./dokuwiki`.

```bash
$ tree dokuwiki
dokuwiki/
├── Chart.lock
├── charts
│   └── common
│       ├── Chart.yaml
│       ├── README.md
│       ├── templates
│       │   ├── _affinities.tpl
│       │   ├── _capabilities.tpl
│       │   ├── _errors.tpl
│       │   ├── _images.tpl
│       │   ├── _ingress.tpl
│       │   ├── _labels.tpl
│       │   ├── _names.tpl
│       │   ├── _secrets.tpl
│       │   ├── _storage.tpl
│       │   ├── _tplvalues.tpl
│       │   ├── _utils.tpl
│       │   ├── validations
│       │   │   ├── _cassandra.tpl
│       │   │   ├── _mariadb.tpl
│       │   │   ├── _mongodb.tpl
│       │   │   ├── _postgresql.tpl
│       │   │   ├── _redis.tpl
│       │   │   └── _validations.tpl
│       │   └── _warnings.tpl
│       └── values.yaml
├── Chart.yaml
├── ci
│   └── ct-values.yaml
├── README.md
├── templates
│   ├── deployment.yaml
│   ├── dokuwiki-pvc.yaml
│   ├── extra-list.yaml
│   ├── health-ingress.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── ldap-daemon-secrets.yaml
│   ├── NOTES.txt
│   ├── secrets.yaml
│   ├── svc.yaml
│   └── tls-secrets.yaml
├── values.schema.json
└── values.yaml



5 directories, 30 files
```

You the kubernetes resources that are generated using the Go template language are to be found in `./dokuwiki/templates`.


## Installing the chart
We will use the values from `helm/dokuwiki-sample.yaml` to configure the chart.
Install the helm chart:

```
helm upgrade --install doku bitnami/dokuwiki -f values-dokuwiki-sample.yaml
```

View the chart's status:
```
$ helm ls
NAME            NAMESPACE               REVISION        UPDATED                                         STATUS          CHART           APP VERSION 
doku            jerry-b-anderson        1               2021-08-27 05:29:11.140439024 +0200 CEST        deployed        dokuwiki-11.2.5 20200729.0.0
```

Now let's change an arbitrary value in the file `value-dokuwiki-sample.yaml` and then update the chart:
```
$ helm upgrade --install doku bitnami/dokuwiki -f values-dokuwiki-sample.yaml
```

...well this didn't work... lets try it again with the password suggested by the error message
```
$ helm upgrade --install doku bitnami/dokuwiki -f values-dokuwiki-sample.yaml --set dokuwikiPassword=solarwinds123
```
```
$ helm ls
NAME            NAMESPACE               REVISION        UPDATED                                 STATUS          CHART           APP VERSION 
doku            jerry-b-anderson        2               2021-08-27 05:34:47.90099986 +0200 CEST deployed        dokuwiki-11.2.5 20200729.0.0
```

You can of course undo a change we did
```
$ helm rollback doku 1
Rollback was a success! Happy Helming!
```

Let's see what happened:
```
$ helm history doku
REVISION        UPDATED                         STATUS          CHART           APP VERSION     DESCRIPTION     
1               Fri Aug 27 05:38:26 2021        superseded      dokuwiki-11.2.5 20200729.0.0    Install complete
2               Fri Aug 27 05:38:32 2021        superseded      dokuwiki-11.2.5 20200729.0.0    Upgrade complete
3               Fri Aug 27 05:38:57 2021        deployed        dokuwiki-11.2.5 20200729.0.0    Rollback to 1   
```


## Create your own helm chart

Using helm charts should be your new standard way of deploying longer lived applications. So let's practice creating our own.
Create a new helm chart:

```
$ helm create testchart
Creating testchart
```

Helm will create a new directory `testchart` and create some really usefull defaults.

```
$tree testchart
testchart/
├── charts
├── Chart.yaml
├── templates
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── NOTES.txt
│   ├── serviceaccount.yaml
│   ├── service.yaml
│   └── tests
│       └── test-connection.yaml
└── values.yaml
```

## Exercise
Change the chart and its values, so that it deploys `gcr.io/google-samples/hello-app:1.0` and makes it accessible
 via an ingress.
 
 ---
 
 **Note**: 
 You can install your chart using `helm upgrade --install RELEASENAME CHARTFOLDER -f VALUES_FILE`
 
 ---
 
 :warning: **the image `hello-app` uses port `8080`**
