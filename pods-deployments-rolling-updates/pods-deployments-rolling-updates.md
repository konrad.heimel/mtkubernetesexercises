# Deployments, Pods and Containers

### Creating Deployments with `create`

Let's start by creating a deployment. Usually, you will use the yaml files called _manifest_, we will experiment with later.
But knowing how  to use imperative `kubectl` commands can come in quite handy.

```
$ kubectl create deployment hello --image=gcr.io/google-samples/hello-app:1.0
```

Let's look at what happened:
```
$ kubectl get pods -w
NAME                       READY   STATUS              RESTARTS   AGE
hello-7fbb7748cc-2fkz6     0/1     ContainerCreating   0          3s
hello-7fbb7748cc-2fkz6     1/1     Running             0          10s
...
```

> **Note**: You can exit `--watch/-w` with `ctrl + c`

---

But of course not only a pod was created:

```
$ kubectl get deployment,replicaset,pod 
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello     1/1     1            1           6m28s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-7fbb7748cc     1         1         1       6m28s

NAME                           READY   STATUS    RESTARTS   AGE
pod/hello-7fbb7748cc-2fkz6     1/1     Running   0          6m29s
```

You can use the `describe` command to describe every object in detail and most importantly view the related events:

```
$ kubectl describe pods hello-7fbb7748cc-2fkz6
Name:         hello-7fbb7748cc-2fkz6
Namespace:    test
Priority:     0
Node:         aks-npld4v4-12345678-vmss000000/192.168.0.35
Start Time:   Wed, 28 Oct 2020 13:36:47 +0100

...

Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  13m   default-scheduler  Successfully assigned adminer/hello-86c57db685-4lrw8 to aks-agentpool-28126975-vmss000000
  Normal  Pulling    13m   kubelet            Pulling image "hello"
  Normal  Pulled     13m   kubelet            Successfully pulled image "hello"
  Normal  Created    12m   kubelet            Created container hello
  Normal  Started    12m   kubelet            Started container hello
...
```

### Finally remove the deployment again
```
$ kubectl delete deploy hello
deployment.apps "hello" deleted
```


## Creating a deployment using declarative configuration files

To create the one or more objects that are declared in a file, just run

```
$ kubectl apply -f echo-simple-deployment.yaml
deployment.apps/echo created
```

Let's look at what was created

```
$ kubectl get deploy,pods,replicaset
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/echo      1/1     1            1           9s

NAME                           READY   STATUS    RESTARTS   AGE
pod/echo-6476cc558f-k2hfn      1/1     Running   0          9s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/echo-6476cc558f      1         1         1       9s
```

> **Note**: The pod created in the previous step still exists. Another pod was created additionally.
---

Now let's talk to our echo service, by port-forwarding _in a separate terminal window_:

```
$ kubectl port-forward deployment/echo 8080:8080
Forwarding from 127.0.0.1:8080 -> 8080
Forwarding from [::1]:8080 -> 8080
Handling connection for 8080
```

> **Note**: Make sure to exit `kubectl port-forward` using `CTRL+C`

---

Is somebody out there?

```
$ curl localhost:8080
CLIENT VALUES:
client_address=127.0.0.1
command=GET
real path=/
query=nil
request_version=1.1
request_uri=http://localhost:8080/

SERVER VALUES:
server_version=nginx: 1.10.0 - lua: 10001

HEADERS RECEIVED:
accept=*/*
host=localhost:8080
user-agent=curl/7.68.0
BODY:
```

Now change the value of `replicas: 1` to `replicas: 2` and apply the file again:
```
k get deploy,pods,replicaset
NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/echo      1/2     2            1           100s

NAME                           READY   STATUS              RESTARTS   AGE
pod/echo-6476cc558f-2lxr9      0/1     ContainerCreating   0          9s
pod/echo-6476cc558f-k2hfn      1/1     Running             0          100s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/echo-6476cc558f      2         2         1       100s
```

Lets observe what happens, if we kill one of the pods:

```
$ kubectl delete pods echo-6476cc558f-k2hfn
pod "pods echo-6476cc558f-k2hfn" deleted
```

See how the replicaset made sure that another pod was spawned automatically:
```
$ kubectl get pods
NAME                       READY   STATUS        RESTARTS   AGE
echo-6476cc558f-57ssh      1/1     Running       0          3s
echo-6476cc558f-v7zzv      0/1     Terminating   0          55s
```

You can also scale up or down a deployment using kubectl
```
$ kubectl scale deploy echo  --replicas=4
```

```
$ kubectl get deploy
NAME      READY   UP-TO-DATE   AVAILABLE   AGE
echo      4/4     4            4           10m
```

Scaling down to zero is possible too:
```
$ kubectl scale deploy echo  --replicas=0
```
```
$ kubectl get pods
NAME                       READY   STATUS        RESTARTS   AGE
echo-6476cc558f-4qth6      0/1     Terminating   0          11m
echo-6476cc558f-57ssh      0/1     Terminating   0          10m
echo-6476cc558f-j5nzw      0/1     Terminating   0          105s
```

# Rolling Updates, Stateful Set
## Create Deployment

Create a deployment with multiple replicas
```
kubectl create deployment nginx --image=nginx:1.7.9
kubectl expose deployment nginx --port 80 --type LoadBalancer
kubectl scale deployment nginx --replicas=4
```

Note down the loadbalancer IP from the service we just generated
```
kubectl get service
```

## Watch the server's version and pods
Open another terminal and paste the following command to continously query the image version:
```
$ while true; do  curl -sI  20.54.217.100  | grep Server; sleep 1; done
Server: nginx/1.7.9
Server: nginx/1.7.9
...
```
You can  continuously watch the pods with the `-w` flag or with
```
$ watch -n 1 kubectl get pods
```
---
**Note**: 
You can exit `watch` with `ctrl + c`
---

## Update the deployment

Now change the deployment's image version:
```
$ kubectl set image deployment/nginx nginx=nginx:1.9.1 --record
```

> **Note**: You can specify the --record flag to write the command executed in the resource annotation kubernetes.io/change-cause.
> The recorded change is useful for future introspection. For example, to see the commands executed in each Deployment revision.

---

Watch how the pods are being replaced one by one without causing any outage.

You can watch the rollout status using:
```
$ kubectl rollout status deployment nginx
deployment "nginx" successfully rolled out
```
The rollout history is accessible using:

```
$ kubectl rollout history deployment nginx
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl set image deployment/nginx nginx=nginx:1.9.1 --record=true
```
Next, let's try out what happens in case of a failing deployment.

```
$ kubectl set image deployment/nginx nginx=nginx:na --record=true
```

We can undo this using:
```
$ kubectl rollout undo deployment nginx
```
## Export a resource to yaml
Save the current state of the deployment into a yaml file to make editing easier. 
```
$ kubectl get deployment nginx -o yaml > deployment.yaml
```

<!-- You can install the `krew` `neat` plugin to remove unneeded fields from the output:
```
$ kubectl krew install neat
Updated the local copy of plugin index.
Installing plugin: neat
Installed plugin: neat
\
 | Use this plugin:
 |      kubectl neat
 | Documentation:
 |      https://github.com/itaysk/kubectl-neat
/
WARNING: You installed a plugin from the krew-index plugin repository.
   These plugins are not audited for security by the Krew maintainers.
   Run them at your own risk.
```

Now you can automatically clean up the output of the above command:
```
$ kubectl get deployment nginx -o yaml | kubectl neat > deployment.yaml
```-->

In case `kubectl apply -f` fails, you can update the resource using:
```
$ kubectl replace -f deployment.yaml
```
## StrategyType: Recreate
Change the field 
```
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
```
to
```
 strategy:
    type: Recreate
```

in the file created using the method described before.
Now reapply the failed update and observe what happens:

```
$ kubectl replace -f deployment.yaml
$ kubectl set image deployment/nginx nginx=nginx:na --record=true
```

> **Note**: `Recreate` can be useful in debug environments, but is mostly unsuited for other purposes.

---

## Replicasets

Do you remember how Deployments perform rolling updates?
Lets check the ReplicaSets that have been created in the meantime.

```sh
$ kubectl get replicaset
```

## Clean up

Finally let's clean up again:

```
$ kubectl delete -f echo-simple-deployment.yaml 
deployment.apps "echo" deleted
$ kubectl delete deployment nginx
$ kubectl delete service nginx
```
