# Storage

## EmptyDir

First let's look at the deployment defined in 'deployment-emptyDir.yaml'. There is an `initContainer`, that runs before
the main container and creates a touches with on a shared `emptyDir` volume.
```
      initContainers:
        - name: init-hello
          image: busybox:1.28
          volumeMounts:
            - mountPath: /cache
              name: cache-volume
          args:
            - /bin/sh
            - -c
            - touch /cache/"$(date +%s)"-hello-from-initcontainer;
```

And then there is a main container, that also just touches a file.

```
         ...
          volumeMounts:
            - mountPath: /cache
              name: cache-volume
          args:
            - /bin/sh
            - -c
            - ls /cache/; touch /cache/"$(date +%s)";
```
---
**Question**

What do you think will be the output of `ls /cache`?

---
Let's first begin watching the pods:
```
$ kubectl get pods -w
```

Next, let's apply the file:
```
$ kubectl apply -f deployment-emptyDir.yaml
deployment.apps/empty-dir-demo created
```

---
**Question**

Why is the pod in a crashloop?

---

Look at the pods logs for a history of which containers where started.
```
$ kubectl logs deploy/empty-dir-demo
```

Now delete the first pod of the deployment:

```
$ kubectl delete pod $(kubectl get pod -l app=empty-dir-demo -o jsonpath="{.items[0].metadata.name}")
pod "empty-dir-demo-5d8cfb59d8-gtgq9" deleted
```

And look at the logs again:
```
$ kubectl logs deploy/empty-dir-demo
```

## Cleanup
```
$ kubectl delete deploy/empty-dir-demo
```
# PersistentVolumes

Lets first list the available storage classes:

```
$ kubectl get storageclass
NAME                PROVISIONER                RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
azurefile           kubernetes.io/azure-file   Delete          Immediate           true                   25h
azurefile-premium   kubernetes.io/azure-file   Delete          Immediate           true                   25h
default (default)   kubernetes.io/azure-disk   Delete          Immediate           true                   25h
managed-premium     kubernetes.io/azure-disk   Delete          Immediate           true                   25h
```

Usually, PersistentVolumes are created indirectly using a `PersistentVolumesClaim`. Let's create one:
```
$ kubectl apply -f pvc-test.yaml
persistentvolumeclaim/pvc-test created
```

First, the `PersistentVolumeClaim` will be in status `Pending`.

```
$ kubectl get pvc
NAME                   STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-test               Pending                                                                        default        1s
pvc-test               Bound     pvc-c15f23ba-d9fc-4dd1-9013-507d7a554b82   1Gi        RWO            default        42s
```

Why is it `Pending`? 

```
$ kubectl describe pvc pvc-test 
Events:
  Type    Reason                Age               From                         Message
  ----    ------                ----              ----                         -------
  Normal  WaitForFirstConsumer  7s (x3 over 32s)  persistentvolume-controller  waiting for first consumer to be created before binding
```

Lets try to create a consumer
```
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```
Lets look at the pvc again
```
$ kubectl get pvc
NAME       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
pvc-test   Bound    pvc-fc6bb25e-4c18-4c3d-92db-05b8df13b7a1   1Gi        RWO            default        71s
```
We can also look at the associated `PersistentVolumes`:

```
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                          STORAGECLASS   REASON   AGE
pvc-c15f23ba-d9fc-4dd1-9013-507d7a554b82   1Gi        RWO            Delete           Bound    default/pvc-test               default                 4m49s
```

---
**Question**

What do you think will happen, after we delete the `PersistentVolumeClaim`?

---

First, watch the `PersistentVolumes` in a separate terminal:
```
$ kubectl get pv -w
```

Let's find out and frist delete the consumer:
```
$ kubectl delete -f deployment-pvc.yaml
deployment.apps "pvc-demo" deleted
```
```
$ kubectl delete -f pvc-test.yaml
persistentvolumeclaim "pvc-test" deleted
```

Now see if the PersistentVolumes is still there(this may take a few seconds) 
```
$ kubectl get pv
No resources found
```

Recreate the `PersistentVolumeClaim`.
```
$ kubectl apply -f ./pvc-test.yaml
persistentvolumeclaim/pvc-test created
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```

But this time, let's make sure out data doesn't get lost when we delete it:
```
$ kubectl patch pv $(kubectl get pvc pvc-test -o jsonpath="{.spec.volumeName}") -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
persistentvolume/pvc-55cfeef7-3bbf-484c-8473-543e5b22942c patched
```

The `ReclaimPolicy` is now set to `Retain`.
```
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                          STORAGECLASS   REASON   AGE
pvc-55cfeef7-3bbf-484c-8473-543e5b22942c   1Gi        RWO            Retain           Bound    default/pvc-test               default                 2m53s
```

Let's delete the `PersistentVolumeClaim` and the comsumer again.
kubectl delete -f deployment-pvc.yaml
```
$ kubectl delete -f deployment-pvc.yaml
deployment.apps "pvc-demo" deleted
$ kubectl delete -f ./pvc-test.yaml
persistentvolumeclaim "pvc-test" deleted
```

The `PersistentVolumec` is `Released` but not deleted:
```
$ kubectl get pv
NAME                                       CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS     CLAIM                          STORAGECLASS   REASON   AGE
pvc-55cfeef7-3bbf-484c-8473-543e5b22942c   1Gi        RWO            Retain           Released   default/pvc-test               default                 4m23s
```

When we create the `PersistentVolumeClaim` again, it will be associated with it again.
```
$ kubectl apply -f pvc-test.yaml
persistentvolumeclaim/pvc-test created
$ kubectl apply -f deployment-pvc.yaml
deployment.apps/pvc-demo created
```

Once it has crashed at least once, delete a pod again and look at the output:
```
$ kubectl delete pod $(kubectl get pod -l app=pvc-demo -o jsonpath="{.items[0].metadata.name}")
  pod "pvc-demo-7899dcc6c6-bh4s9" deleted
```

Look at the newly created pods output:
```
$ kubectl logs deploy/pvc-demo
```

## Clean up
```
$ kubectl patch pv $(kubectl get pvc pvc-test -o jsonpath="{.spec.volumeName}") -p '{"spec":{"persistentVolumeReclaimPolicy":"Delete"}}'
$ kubectl delete deploy empty-dir-demo pvc-demo
$ kubectl delete pvc pvc-test
```

