## `kubectl` 101
Kubectl is by far the most important tool when interacting with Kubernetes Clusters. 
It is written in go and can be extended with a huge collection of plugins.

All the information needed to authenticate with a cluster is managed inside the `kubeconfig` file. It contains information 
about users, namespaces, clusters and authentication mechanism. The `kubeconfig` file represents a sort of local state of `kubectl`.

---
**Note**: A `kubeconfig` can contain information about multiple clusters represented as _contexts_.

---

### View the kubeconfig

Your kubeconfig is usually located in `~/.kube/config` or set using the environment variable `KUBECONFIG`.

Let's start by viewing your kubeconfig:
```bash
$ kubectl config view
```

First check if you can connect to the cluster:

```bash
$ kubectl cluster-info

Kubernetes master is running at https://test-aks-devqa-dns-841c8351.hcp.westeurope.azmk8s.io:443
addon-http-application-routing-default-http-backend is running at https://test-aks-devqa-dns-841c8351.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/addon-http-application-routing-default-http-backend/proxy
addon-http-application-routing-nginx-ingress is running at http://52.122.31.89:80 http://52.122.31.89:443 
CoreDNS is running at https://test-aks-devqa-dns-841c8351.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://test-aks-devqa-dns-841c8351.hcp.westeurope.azmk8s.io:443/api/v1/namespaces/kube-system/services/https:metrics-server:/proxy
```

### Looking at the nodes

Next, maybe look at the cluster's nodes:
```bash
$ kubectl get nodes

NAME                                STATUS   ROLES   AGE    VERSION
aks-agentpool-12345678-vmss000000   Ready    agent   299d   v1.16.15
aks-nplb4ms-12345678-vmss000000     Ready    agent   103d   v1.16.15
aks-nplb4ms-12345678-vmss000001     Ready    agent   103d   v1.16.15
aks-npld4v4-12345678-vmss000000     Ready    agent   22d    v1.16.15
aks-npld4v4-12345678-vmss000001     Ready    agent   22d    v1.16.15
```

Adding `-o wide` also shows the nodes ip addresses
```
$ kubectl get nodes -o wide

NAME                                STATUS   ROLES   AGE    VERSION    INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
aks-agentpool-12345678-vmss000000   Ready    agent   299d   v1.16.15   192.168.0.4     <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-nplb4ms-12345678-vmss000000     Ready    agent   103d   v1.16.15   192.168.0.159   <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-nplb4ms-12345678-vmss000001     Ready    agent   103d   v1.16.15   192.168.0.190   <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-npld4v4-12345678-vmss000000     Ready    agent   22d    v1.16.15   192.168.0.35    <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-npld4v4-12345678-vmss000001     Ready    agent   22d    v1.16.15   192.168.0.126   <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
```
---

**Note**: On preconfigured kubernetes clusters like `aks`,`gke` or `aws`, only the worker nodes will be visible and 
accessible.

---

Show node memory usage with:

```
$ kubectl top nodes
NAME                                        CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
rz-vsv-op-02.local.test.de    1820m        11%    6725Mi          21%       
rz1-vsv-op-03.local.test.de   1633m        21%    19543Mi         83%       
rz1-vsv-op-05.local.test.de   609m         8%     3757Mi          50%       
rz2-vsv-op-04.local.test.de   1266m        16%    12486Mi         53%       
rz2-vsv-op-06.local.test.de   565m         7%     3807Mi          51%       
```

## Create your own namespace

A namespace provide scopes for names and are for example used to apply rules to a group of ressources.

First list all available namespaces:

```bash
$ kubectl get ns
NAME              STATUS   AGE
default           Active   35m
kube-networking   Active   13m
kube-node-lease   Active   35m
kube-public       Active   35m
kube-system       Active   35m
...
```

Creating a namespace is easy...
```bash
$ kubectl create namespace jerry-b-anderson
```

..but switching the default namespace takes a relatively long command. You wont need to memorize this command, but please make sure,
you switch from the default namespace.

```bash
$ kubectl config set-context --current --namespace=jerry-b-anderson
```

Finally, please validate the context switch worked

```bash
$ kubectl config view --minify | grep namespace:
    namespace: jerry-b-anderson
```
