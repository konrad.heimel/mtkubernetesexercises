# Challenge 1 - Mysql the Hard Way

Objective:
* Setup `mysql`
* Setup `adminer`

Requirements:
* Create a deployment for the image `mariadb`.
* Set the `MYSQL_ROOT_PASSWORD` env variable from a secret
* Create a persistant volume and mount it under `/var/lib/mysql`
* Create a deployment for the image `adminer` and make it accessible
* Connect to your `mysql` service using `adminer`

