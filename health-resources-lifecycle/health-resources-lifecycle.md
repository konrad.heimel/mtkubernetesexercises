# Health, Resources, Lifecycle
## Liveness probe

Health probes are considered an importand best practice. Usually a liveness- or readyness Probe consists of a call
to an api address, but different types are also possible, like e.g. bash commands.

Read the file `pod-liveness.yaml` and created the pod it defines:
```
$ kubectl apply -f pod-liveness.yaml
```

When the container starts, it runs the following commands:

```
touch /tmp/healthy; 
sleep 30; 
rm -rf /tmp/healthy;
sleep 600;
```

If one only hat to consider these commands, the container would stop running after 10:30 minutes.

Look at the pods events:
```
$ kubectl describe pod liveness-demo
...
Events:
  Type     Reason     Age   From               Message
  ----     ------     ----  ----               -------
  Normal   Scheduled  35s   default-scheduler  Successfully assigned adminer/liveness-demo to aks-npld4v4-28126975-vmss000001
  Normal   Pulling    34s   kubelet            Pulling image "k8s.gcr.io/busybox"
  Normal   Pulled     33s   kubelet            Successfully pulled image "k8s.gcr.io/busybox"
  Normal   Created    33s   kubelet            Created container liveness
  Normal   Started    33s   kubelet            Started container liveness
  Warning  Unhealthy  2s    kubelet            Liveness probe failed: cat: can't open '/tmp/healthy': No such file or directory
```
The pod will get restarted after a certain time.
```
$ kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
liveness-demo                 1/1     Running   1          76s
```
## Readyness probe
Now let's look at the file `pod-readyness.yaml` and start it up.
```
$ kubectl apply -f pod-readyness.yaml
```

Look at the pods status:
```
$ kubectl get pods
NAME                       READY   STATUS    RESTARTS   AGE
readyness-demo             0/1     Running   0          37s
```

Otherwise, the pod seems to be doing just fine:
```
$ kubectl logs -f readyness-demo1
Still alive...
Still alive...
Still alive...
Still alive...
Still alive...
Still alive...
```

---
**Exercise**

Open a shell on the pod `readyness-demo` and create the missing file to get the pod into status `ready`.

---

## Startup Probe
The pod defined in `pod-needing-startup-probe` only answers it's liveness probes after more than 30 seconds,
which results in a crash loop. Pods not starting up immediately is a very common behaviour when deployments are waiting for e.g. databases to respond.
The answer to this problem is not to set the `initialDelay` to a very high value, but to either use the recently introduced
startup probe or an `initcontainer`.

```
$ kubectl apply -f pod-needing-startup-probe.yaml 
```

```
$ kubectl describe pods lifecycle-needs-startup
  Type     Reason     Age                     From               Message
  ----     ------     ----                    ----               -------
  Normal   Scheduled  7m23s                   default-scheduler  Successfully assigned adminer/lifecycle-needs-startup to aks-nplb4ms-28126975-vmss000000
  Normal   Pulling    7m22s                   kubelet            Pulling image "gcr.io/google-samples/hello-app:1.0"
  Normal   Pulled     7m20s                   kubelet            Successfully pulled image "gcr.io/google-samples/hello-app:1.0"
  Normal   Killing    6m35s (x2 over 7m10s)   kubelet            Container lifecycle-needs-startup failed liveness probe, will be restarted
  Warning  Unhealthy  6m13s (x10 over 7m13s)  kubelet            Readiness probe failed: Get http://192.168.0.166:8080/: dial tcp 192.168.0.166:8080: connect: connection refused
  Normal   Created    6m5s (x3 over 7m20s)    kubelet            Created container lifecycle-needs-startup
  Normal   Started    6m5s (x3 over 7m19s)    kubelet            Started container lifecycle-needs-startup
  Normal   Pulled     6m5s (x2 over 6m40s)    kubelet            Container image "gcr.io/google-samples/hello-app:1.0" already present on machine
  Warning  Unhealthy  6m (x3 over 7m10s)      kubelet            Liveness probe failed: Get http://192.168.0.166:8080/: dial tcp 192.168.0.166:8080: connect: connection refused
  Warning  BackOff    2m15s (x5 over 4m15s)   kubelet            Back-off restarting failed container
```

---
**Exercise**

Familiarize yourself with [startup probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-startup-probes)  
or [init containers](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)  and alter the manifest `pod-needing-startup-probe.yaml` so that it shows a `ready` status.

---

## Resources
You can view a pods current resource usage with:
```
kubectl top pod liveness-demo
```

Let's look at what happens to a pod having a resouce limit set, that is too small:

```
$ kubectl describe pod no-resources-demo
Events:
  Type     Reason                  Age                From               Message
  ----     ------                  ----               ----               -------
  Normal   Scheduled               55s                default-scheduler  Successfully assigned adminer/lifecycle-demo to aks-nplb4ms-28126975-vmss000000
  Normal   SandboxChanged          4s (x12 over 51s)  kubelet            Pod sandbox changed, it will be killed and re-created.
  Warning  FailedCreatePodSandBox  1s (x13 over 52s)  kubelet            Failed create pod sandbox: rpc error: code = Unknown desc = failed to start sandbox container for pod "lifecycle-demo": Error response from daemon: OCI runtime create failed: container_linux.go:370: starting container process caused: process_linux.go:338: getting the final child's pid from pipe caused: read init-p: connection reset by peer: unknown
```
---

**Note**: 
It is considered a important best practice to set a pods resources using the `requests` and `limits` fields. Otherwise, a failing pod can
pull down a whole node and with it a cluster.

---

Let's see what happens, when we try to create a pod that exceeds our namespaces resource quota:
```
$ kubectl apply -f pod-hungry.yaml 
Error from server (Forbidden): error when creating "pod-liveness.yaml": pods "liveness-demo" is forbidden: failed quota: mem-cpu-demo: must specify limits.cpu,limits.memory,requests.cpu,requests.memory
```

When there are no resouce quotas set for your namespace, you might also see something like this, when the cluster is out of resources:
```
Events:
  FirstSeen     LastSeen        Count   From                    SubObjectPath   Type            Reason                  Message
  ---------     --------        -----   ----                    -------------   --------        ------                  -------
  24m           0s              89      default-scheduler                       Warning         FailedScheduling        No nodes are available that match all of the following predicates:: Insufficient cpu (3).
```

## Lifecycle Handlers
In the file `pod-lifecycle-events`, you can see that the postStart command writes a message file to the Container's `/usr/share` directory. 
The preStop command shuts down nginx gracefully. 
This can helpful if the Container is being terminated because of a failure.

Create the pod:
```
$ kubectl apply -f pod-lifecycle-events.yaml
```

Verify that the pod is running and verify the contents of `/usr/share/message`:
```
$ kubectl exec -it lifecycle-demo -- cat /usr/share/message
Hello from the postStart handler
```

