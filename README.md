## Kubernetes Workshop Exercises

## Table of Content

[Cheat Sheet](cheatsheet.md)

---

#### Exercises

* [Setup and Basics](setup-kubectl-basics/kubectl-basics.md)
* [Pods, Deployments, Rolling Updates](./pods-deployments-rolling-updates/pods-deployments-rolling-updates.md)
* [Services](services/services.md)
* [Ingresses with Nginx Ingress Controller](./ingress-nginx/ingress-nginx.md)
* [Environment Variables, Configmaps, Secrets](./env-vars-configmaps-secrets/configmaps-secrets-env.md)
* [Persistent Storage](./persistent-storage/persistent-storage.md)
* [Helm Charts](./helm/helm-chart.md)

---

#### Challenges

* [Bug Hunt](challenges/bug-hunt/bug-hunt.md)
* [Deploy Wordpress with Helm](./challenges/beginner-wordpress-helm/wordpress-helm.md)
* [Mysql the Hard Way](./challenges/intermediate-mysql-the-hard-way/mysql-the-hard-way.md)
* [Redmine Helm Chart](./challenges/advanced-redmine-helm/redmine.md)
