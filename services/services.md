# Services

Let's start by creating an arbitrary deployment and our trusty debug pod:

```
$ kubectl create deployment hello --image=gcr.io/google-samples/hello-app:1.0
$ kubectl create deployment debug --image=praqma/network-multitool
```

---
**Note**: praqma/network-multitool is a container image containing lots of tools you might consider basic,
but are in fact often rightfully missing from docker images. (e.g. wget, curl, nslookup, tcpdump, traceroute,
 vi etc.)
---

## ClusterIP Service

Expose the deployment as a ClusterIP service

```
$ kubectl expose deployment hello --port 80 --target-port=8080 --type ClusterIP
```

Look at what was created
```
$ kubectl get svc

NAME      TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
hello     ClusterIP   10.0.89.176   <none>        8080/TCP    2m10s
```

Now open a shell on the pod debug pod

```
$ kubectl exec --stdin --tty deployment/debug -- /bin/sh
bash-5.0# 
```

---
**Note**: 
We used `svc` as abbrevation for `service`. You can list all available resource types and their abbrevations with 
`kubectl api-resources`
---

Try to `curl` the `ClusterIP` of the service, we created previously

```

/# curl -s 10.0.89.176
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

Now try to call the service using DNS.

```
/# curl hello.NAMESPACE.svc.cluster.local
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

Log out of the shell using `exit` or with `CTRL-D`


## NodePort Service

---
 
 **Note**: 
Of course only one Service per Cluster can claim the Port 30007 so please change it to another one in
`nodeport.yaml`  
---

Let's try out the NodePort Servicetype next. Create a nodeport service using a manifest.

```
$ kubectl apply -f nodeport.yaml 
```

```
$ kubectl get svc
NAME             TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
hello            ClusterIP   10.0.89.176   <none>        80/TCP         78m
hello-nodeport   NodePort    10.0.36.207   <none>        80:30007/TCP   89s
```

We now have a cluster-wide port for this deployment on all worker nodes. Knowing the public ip of our nodes, we can directly
communicate with it via this pod from outside the cluster.

```
$ kubectl get nodes -o wide
NAME                                STATUS   ROLES   AGE    VERSION    INTERNAL-IP     EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
aks-nplb4ms-12345678-vmss000001     Ready    agent   104d   v1.16.15   192.168.0.190   <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-npld4v4-12345678-vmss000000     Ready    agent   23d    v1.16.15   192.168.0.35    <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12
aks-npld4v4-12345678-vmss000001     Ready    agent   23d    v1.16.15   192.168.0.126   <none>        Ubuntu 16.04.7 LTS   4.15.0-1096-azure   docker://19.3.12

```

---

**Note**:

If the nodes don't show with an EXTERNAL-IP, you have to use the INTERNAL-IP instead and open a shell on your `debug`
container inside your cluster `kubectl exec --stdin --tty deployment/debug -- /bin/sh`.

---


```
$ curl -s 192.168.0.126:30007
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

## LoadBalancer Service

---
 
 **Note**: 

There is a limit of public ip addresses allowed for each azure account, so not all will be able to succeed in this exercise.

---
 
Now there is the final service type to communicate with a pod, the LoadBalancer Service.
Some cloud providers allow you to specify the loadBalancerIP. In those cases, the load-balancer is created with the user-specified loadBalancerIP. If the loadBalancerIP field is not specified, the loadBalancer is set up with an ephemeral IP address. If you specify a loadBalancerIP but your cloud provider does not support the feature, the loadbalancerIP field that you set is ignored.

```
$  kubectl expose deployment hello --port 80 --target-port 8080 --type LoadBalancer --name hello-loadbalancer
```

Note how the external IP is still showing pending.
```
$ kubectl get svc
NAME                 TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
hello                ClusterIP      10.0.89.176   <none>        8080/TCP         86m
hello-loadbalancer   LoadBalancer   10.0.250.37   <pending>     8080:30587/TCP   12s
hello-nodeport       NodePort       10.0.36.207   <none>        8080:30007/TCP   9m22s
```

In a few minutes you will be able to talk with the service without any port numbers.
```
$ kubectl get svc
NAME                 TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
hello                ClusterIP      10.0.89.176   <none>         8080/TCP         88m
hello-loadbalancer   LoadBalancer   10.0.250.37   20.71.82.154   8080:30587/TCP   90s
hello-nodeport       NodePort       10..36.207   <none>         8080:30007/TCP   10m
```
```
curl 20.71.82.154
Hello, world!
Version: 1.0.0
Hostname: web-557f59c6cf-v54ds
```

 ## ExternalName Service

There is a last service type, that is not used to connect to pods, but for providing different DNS names for external
services. It is often much cleaner to map external services this way, an example might be different external databases for
`dev`, `qa` and `prod`.

Apply the ExternalName Service you find in `external_name.yaml`
```
$ kubectl apply -f external_name.yaml
service/apache created
```
This service maps the website `apache.org` to an internal DNS name. We chose this website because it doesn't use `tsl`.
Open a terminal into your network-multitool container.
```
$ kubectl exec -it deploy/debug -- /bin/bash
bash-5.0#  curl apache | grep \<title\>
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
Dload  Upload   Total   Spent    Left  Speed
<title>Welcome to The Apache Software Foundation!</title>-:-- --:--:--     0
100 85182  100 85182    0     0   602k      0 --:--:-- --:--:-- --:--:--  598k
```
The `apache.org` is now available with the name `apache` in your namespace and with `apache.YOUR_NAMESPACE.svc.cluster.local`
on the whole cluster.

## Clean up
Now remove all those services you created. You can still leave the `debug` deployment, as it will be useful later.

```
$ kubectl delete -f external_name.yaml
service "apache" deleted
$ kubectl delete deploy hello
deployment "hello" deletedhello
$ kubectl delete svc hello-loadbalancer hello-nodeport hello apache
service "hello" deleted
service "apache" deleted
service "hello-nodeport" deleted
service "hello-loadbalancer" deleted
```

